<?php
namespace App\Http\Controllers;

/**
 * Class Transformer
 * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
 * @package App\Transformers
 */
abstract class Transformer
{
    /**
     * @var array
     */
    protected $data = array();

    /**
     * @var object
     */
    protected $inputData;

    /**
     * Transformer constructor.
     * @param $inputData
     */
    public function __construct($inputData)
    {
        $this->inputData = $inputData;
        $this->transform();

    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @return mixed
     */
    abstract protected function transform();

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @return array
     */
    public function getResult()
    {
       return $this->data;
    }

}