<?php

namespace App\Http\Controllers;

use App\Http\Service\MainService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    use ApiResponseHandler;

    public function index(Request $request)
    {

        $input = $request->all();
        $rules = [
            'budget' => 'required|numeric|integer|min:100|max:5000',
            'day' => 'required|numeric|integer|min:1|max:5',
        ];

        // check validation
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return $this->setStatusCode(400)->respondWithError($messages);
        }

        $budget = $input['budget'];
        $day = $input['day'];

        if($budget / $day < 50) {
            return $this->setStatusCode(400)->respondWithError('budget for a day must be at least 50');
        }

        $result = MainService::calculate($budget, $day);

        if($result) {

            $res['totalBudget'] = $budget;
            $res['totalDay'] = $day;
            $res['item'] = $result;
            $mainTransformers = new MainTransformer($res);

            $data = $mainTransformers->getResult();

            return response()->json($data)->setStatusCode(200);

        } else
            return $this->setStatusCode(400)->respondWithError('something is wrong');


    }
}
