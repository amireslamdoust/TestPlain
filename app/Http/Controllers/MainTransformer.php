<?php

namespace App\Http\Controllers;

/**
 * Class BaseTransformer
 * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
 * @package App\Core\Transformer
 */
class MainTransformer extends Transformer
{

	protected function transform()
	{

	    $this->data['summary']['budget'] = $this->inputData['totalBudget'];
	    $this->data['summary']['day'] = $this->inputData['totalDay'];

	    $counter = 0;

	    foreach ($this->inputData['item'] as $key => $value) {
            $time = 0;
	        foreach ($value as $item) {
                $counter++;

                $showTime = "10:00";
                $startTime = 0;
                if($time) {
                    $startTime = ($time) / 60;
                    if(is_int($startTime))
                    {
                        $startTime = 10 + $startTime;
                        $decimal = 30;
                        $showTime = $startTime . ":" . $decimal;

                    } else {

                        $decimal =(int)( ($startTime - (int) $startTime) * 60) + 30;

                        if($decimal == 60)
                        {
                            $startTime = 10 + (int) $startTime + 1;
                            $decimal = "00";
                        } else {
                            $startTime = 10 + (int) $startTime;
                        }
                        $showTime = $startTime . ":" . $decimal;

                    }

                }
                $price = $item['price'];
                $duration = $item['duration'];
                $time += $duration;
                $this->data['schedule'][$key][] = array(
                    'startTime' => $showTime,
                    'duration' => $duration,
                    'price' => $price,
                );


            }

        }

	    $this->data['summary']['activity'] = $counter;
	}

}
