<?php
/**
 * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
 * @since 7/24/18 - 1:05 AM
 */

namespace App\Http\Controllers;

/**
 * Trait ApiResponseHandler
 * based on http://www.restapitutorial.com/httpstatuscodes.html
 * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
 * @package App\Http\Controllers
 */
trait ApiResponseHandler
{

    /**
     * response code
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @var int
     */
    public $statusCode = 200;

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $data
     * @param array $headers
     * @return mixed
     */
    public function respond($data, $headers = [])
    {
        return \Response::json($data, $this->getStatusCode(), $headers);
    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function responseNotFound($message = 'Not Found')
    {
        return $this->setStatusCode(400)->respondWithError($message);
    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param string $message
     * @return mixed
     */
    public function authenticationFailed($message = 'Invalid Credentials')
    {
        return $this->setStatusCode(401)->respondWithError($message);
    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $message
     * @return mixed
     */
    public function respondWithSuccess($message) {

        return $this->respond([
            'message' => $message,
            'status_code' => $this->getStatusCode()
        ]);
    }

    /**
     * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
     * @param $errorMessage
     * @return mixed
     */
    public function respondWithError($errorMessage)
    {
        $errors = $errorMessage;

        return $this->respond([
            'error' => $errors,
            'status_code' => $this->getStatusCode()
        ]);
    }

}