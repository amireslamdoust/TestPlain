<?php
/**
 * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
 * @since 8/15/18 - 5:41 AM
 */

namespace App\Http\Service;


class Sort
{

    public static function sortBy($field, &$array, $direction = 'asc')
    {
        $direction = strtolower(($direction));
        usort($array, create_function('$a, $b', '
            $a = $a["' . $field . '"];
            $b = $b["' . $field . '"];
    
            if ($a == $b) return 0;
    
            return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
        '));

        return true;
    }

}