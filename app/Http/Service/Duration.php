<?php
/**
 * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
 * @since 8/15/18 - 5:58 AM
 */

namespace App\Http\Service;


class Duration
{

    public static function addHalfToDuration(&$input)
    {
        foreach ($input as &$value) {
            $value['duration']+= 30;
        }
    }

    public static function decreaseHalfToDuration(&$input)
    {
        foreach ($input as &$value) {
            foreach ($value as &$row) {
                $row['duration'] -= 30;
            }
        }
    }

}