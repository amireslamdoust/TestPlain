<?php
/**
 * @author Created by Amir Eslamdoust <amireslamdoust@gmail.com>
 * @since 8/15/18 - 5:30 AM
 */

namespace App\Http\Service;

class MainService
{

    public static function calculate($budget, $day)
    {
        $programData = self::_getJson();

        if($programData) {

            Duration::addHalfToDuration($programData);
            Sort::sortBy('price', $programData);

            $program = [];
            $usedId = [];
            $error = false;
            $remainBudget = $budget - ( $day * 50 );
            for($i = 0; $i < $day; $i++) {

                $remainBudget = self::_detectTravel( $remainBudget, $program, $i, $programData, $day, $usedId, $budget, $error);

            }


            if($error)
                return false;

            Duration::decreaseHalfToDuration($program);
            return $program;

        }
        return false;

    }

    private static function _detectTravel($remainBudget, &$program, $key, $programData, $endDay, &$usedId, $totalBudget, &$error)
    {
        $minDayBudget = 50;
        $maxDayBudget = 50 + $remainBudget;
        $day = $key + 1;

        $oldProgram = $programData;

        foreach ($programData as $key => $row) {
            $prices[$key]  = $row['price'];
            $durations[$key] = $row['duration'];
        }

        $halfBudget = (int) ceil($maxDayBudget / 2);
        array_multisort($prices, SORT_DESC, $durations, SORT_DESC, $programData);

        $newProgram = [];
        foreach ($programData as $value) {
            if($value['price'] < $halfBudget) {
                $newProgram[] = $value;
            }
        }

        $program[$day][] = $newProgram[0];
        $usedId[] = $newProgram[0]['id'];

        array_multisort($durations, SORT_ASC, $prices, SORT_ASC, $programData);

        $counter = 2;

        foreach ($programData as $value) {

            if (!in_array($value['id'], $usedId)) {
                $program[$day][] = $value;
                $usedId[] = $value['id'];
                $counter--;
            }
            if($counter == 0)
                break;
        }

        $check = self::_checkDayValid($program[$day], $minDayBudget, $maxDayBudget);

        if(!$check) {



        } else {


            $remainBudget = self::_calculateBudget($program[$day]);

            if($endDay) {

                $totalUsed = self::_totalBudget($program);
                if((int) $totalBudget != (int) $totalUsed) {


                    array_multisort($prices, SORT_DESC, $durations, SORT_DESC, $programData);

                    self::_injectToProgram($program, $totalUsed, $totalBudget, $programData, $usedId);

                    $totalUsed = self::_totalBudget($program);

                    $difference = $totalBudget - $totalUsed;
                    if($difference) {

                        self::_injectDifference($program, $difference, $oldProgram, $usedId, $error);

//                        dd($program);

                    }

                }

            }


        }

    }

    private static function _injectDifference(&$program, $difference, $oldProgram, $usedId, &$error)
    {

        $listPrice = [];

        foreach ($program as $value) {

            foreach ($value as $row) {
                $listPrice[$row['id']] = $row['price'];
            }

        }

        asort($listPrice);
        $list = [];
        foreach ($listPrice as $key => $item) {

            $detectPrice = $item + $difference;

            foreach ($oldProgram as $value) {

                if (!in_array($value['id'], $usedId)) {

                    if($value['price'] == $detectPrice) {
                        $temp = $value;

                        $res = self::_detectDay($program, $key, $temp);
                        if($res) {
                            $program = $res['program'];
                            $list = $value;
                        }

                        break;
                    }

                    if(count($list))
                        break;

                }
                if(count($list))
                    break;

            }
            if(count($list))
                break;

        }

        if(!count($list))
            $error = true;

    }

    private static function _detectDay($program, $id, $temp)
    {
        $day = 0;
        $unseter = 0;
        foreach ($program as $key => $value) {
            foreach ($value as $k => $item) {
                if($item['id'] == $id) {
                    $day = $key;
                    $unseter = $k;
                }
            }
        }

        unset($program[$day][$unseter]);

        $dayTime = self::_calculateDayTime($program[$day]);
        $maxDayTime = 60 * 12;
        if(($dayTime + $temp['duration']) < $maxDayTime) {
            $program[$day][] = $temp;
            return [
                'res' => true,
                'program' => $program
            ];
        } else
            return false;

    }

    private static function _injectToProgram(&$program, &$totalUsed, $totalBudget, $programData, &$usedId)
    {

        $day = count($program);
        $maxDayTime = 60 * 12;
        foreach ($programData as $value) {

            if (!in_array($value['id'], $usedId)) {

                foreach ($program as $day => $row) {
                    $dayDuration = self::_calculateDayTime($row);

                    if(( $value['duration'] + $dayDuration ) < $maxDayTime) {

                        if($totalUsed + $value['price'] < $totalBudget) {

                            $totalUsed += $value['price'];
                            $program[$day][] = $value;
                            $usedId[] = $value['id'];
                        }

                    }

                }

            }

        }



    }

    private static function _calculateDayTime($row)
    {
        $duration = 0;
        foreach ($row as $val) {
            $duration += $val['duration'];
        }

        return $duration;
    }

    private static function _calculateBudget($row)
    {
        $price = 0;
        foreach ($row as $val) {
            $price += $val['price'];
        }

        return $price;
    }

    private static function _totalBudget($program)
    {
        $totalUsed = 0;
        foreach ($program as $pro) {
            foreach ($pro as $item) {
                $totalUsed += $item['price'];

            }
        }
        return $totalUsed;
    }

    private static function _checkDayValid($dayData, $minDayBudget, $maxDayBudget)
    {
        $maxDayTime = 60 * 12;
        $totalTime = 0;
        $totalPrice = 0;
        foreach ($dayData as $value) {
            $totalPrice += $value['price'];
            $totalTime += $value['duration'];
        }

        if($totalTime > $maxDayTime)
            return 'time';

        if($totalPrice > $maxDayBudget)
            return 'max';

        if($totalPrice < $minDayBudget)
            return 'min';

    }

    private static function _getJson($loadFromURL = false)
    {
        try {
            if ($loadFromURL)
                // for if want to load from URL
                $json = json_decode(file_get_contents('http://localhost:8080/'), true);
            else
                // dsfs
                $json = json_decode(file_get_contents('berlin-01.json'), true);

            return $json;
        } catch (\Exception $exception) {
            return false;
        }

    }

}